# OpenML dataset: socmob

https://www.openml.org/d/44837

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This dataset described social mobility, i.e. how the sons' occupations are related to their fathers' jobs.

An instance represent the number of sons that have a certain job A given the father has the job B (additionally conditioned on race and family structure).

The dataset was originally collected for the survey of "Occupational Change in a Generation II"

**Attribute Description**

1. *fathers_occupation*
2. *sons_occupation*
3. *family_structure* - "intact" or "nonintact"
4. *race* - "black" or "white"
5. *counts_for_sons_first_occupation*
6. *counts_for_sons_current_occupation* - target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44837) of an [OpenML dataset](https://www.openml.org/d/44837). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44837/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44837/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44837/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

